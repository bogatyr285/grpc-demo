# gRPC golang sample
gRPC in Go with a simple working example.
### Intallation
Use the following command to install gRPC.

```
go get -u google.golang.org/grpc
```
Install the protoc plugin for Go
```
go get -u github.com/golang/protobuf/protoc-gen-go
```
Or do everything with `make`
```
make install
```

### Compiling the proto file
```
protoc -I proto/ --go_out=plugins=grpc:proto proto/msgs.proto 
```
Or do everything with `make`
```
make generate
```

### Make test
```
make test
```

##### Additional
Generate self-signed TLS certs:
```
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out MyCertificate.crt -keyout MyKey.key
```