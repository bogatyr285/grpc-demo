package client

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"time"

	pb "bitbucket.org/bogatyr285/grpc_demo/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

// Client - structure describes client gRPC props
type Client struct {
	Addr string //Full addr with port
	Conn pb.MsgsServiceClient
}

// Start - run the gRPC client
func (c *Client) Start() {
	creds, err := credentials.NewClientTLSFromFile("cert/server.crt", "")
	if err != nil {
		log.Fatalf("could not load tls cert: %s", err)
	}

	conn, err := grpc.Dial(c.Addr, grpc.WithTransportCredentials(creds))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}

	grpcClnt := pb.NewMsgsServiceClient(conn)
	c.Conn = grpcClnt
}

// NewClient - initialize a new gRPC client instance
func NewClient(addr string) (*Client, error) {
	var client Client

	client.Addr = addr
	return &client, nil
}

func (c Client) SendMsg(senderName, msgToSend, authToken string) (pb.MsgReply, error) {
	md := metadata.Pairs("authorization", authToken)
	ctxWithTimeout, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	reqCtx := metadata.NewOutgoingContext(ctxWithTimeout, md)

	reply, err := c.Conn.SendMsg(reqCtx, &pb.MsgRequest{From: senderName, Body: msgToSend})
	if err != nil {
		return pb.MsgReply{}, errors.New("Error sending message: " + err.Error())
	}
	return *reply, nil
}

func (c Client) GetStreamRes(req string) (chan string, chan error) {
	resChan := make(chan string)
	errChan := make(chan error)
	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
		defer cancel()

		resStream, err := c.Conn.SendStreamMsgs(ctx, &pb.StreamMsgRequest{Body: req})
		if err != nil {
			errChan <- errors.New("Error sending message: " + err.Error())
		}
		for {
			msg, err := resStream.Recv()
			if err == io.EOF { //end of server stream
				errChan <- nil
				break
			}
			if err != nil {
				errChan <- errors.New("Error reading the stream: " + err.Error())
				break
			}
			resChan <- msg.GetMessage()
		}
	}()
	return resChan, errChan
}

func (c Client) SendStreamReq(reqs []*pb.StreamMsgRequest) *pb.StreamMsgResponse {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	stream, err := c.Conn.SendLotsOfMsgs(ctx)
	if err != nil {
		log.Fatalf("Err client streaming: %v", err)
	}

	for _, req := range reqs {
		log.Printf("[SendStreamReq]Client is sending: %+v", req)
		req := &pb.StreamMsgRequest{Body: fmt.Sprintf("Client req is: %+v", req)}
		stream.Send(req)
		time.Sleep(500 * time.Millisecond)
	}
	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("Err getting server answer: %v", err)
	}
	return res
}
