package main

import (
	"flag"
	"fmt"
	"log"

	Client "bitbucket.org/bogatyr285/grpc_demo/client/api"
	pb "bitbucket.org/bogatyr285/grpc_demo/proto"
)

func main() {
	srvAddr := flag.String("addr", "localhost:10000", "Address to connect")
	msgToSend := flag.String("m", "Test gRPC message", "What message to send")
	senderName := flag.String("who", "anon", "Introduce yourself")

	flag.Parse()

	client, err := Client.NewClient(*srvAddr)
	if err != nil {
		log.Fatalf("Failed to inialize gRPC client: %v", err)
	}
	client.Start()

	//unary req
	authToken := "valid-token"
	unaryReply, err := client.SendMsg(*senderName, *msgToSend, authToken)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Unary request got answer from the server: %+v", unaryReply)

	//init server streaming
	streamResChan, errChan := client.GetStreamRes("req to stream")
	if err != nil {
		log.Fatal(err)
	}

getStreamLoop:
	for {
		select {
		case msg := <-streamResChan:
			log.Printf("GetStreamRes received message:%+v\n", msg)
		case err := <-errChan:
			if err != nil {
				log.Printf("GetStreamRes error:%+v\n", err)
			} else {
				log.Println("End of stream")
			}
			break getStreamLoop
		}
	}

	//prepare client streaming
	clntReqs := []*pb.StreamMsgRequest{}
	for i := 0; i < 3; i++ {
		clntReqs = append(clntReqs, &pb.StreamMsgRequest{Body: fmt.Sprintf("Client req is:%d", i)})
	}
	//client streaming
	replyToStream := client.SendStreamReq(clntReqs)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Server's answer to client stream %+v", replyToStream)
}
