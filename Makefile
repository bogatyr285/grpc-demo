install:
	go get -u \
		google.golang.org/grpc \
		github.com/golang/protobuf/protoc-gen-go
		
generate:
	protoc -I proto/ --go_out=plugins=grpc:proto proto/msgs.proto 

test:
	go test -v ./...