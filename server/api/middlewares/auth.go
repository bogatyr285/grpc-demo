package middlewares

import (
	"context"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

// UnaryAuthInterceptor - Auth for unary requests
func UnaryAuthInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	if err := authorize(ctx); err != nil {
		return nil, err
	}
	return handler(ctx, req)
}

// StreamAuthInterceptor - Auth for stream requests
func StreamAuthInterceptor(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	if err := authorize(stream.Context()); err != nil {
		return err
	}
	return handler(srv, stream)
}

//authorize - perform creds validation. Do whatever you want
func authorize(ctx context.Context) error {
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		if len(md["authorization"]) > 0 && md["authorization"][0] == "valid-token" {
			return nil
		}
		return grpc.Errorf(codes.Unauthenticated, "Invalid token")
	}
	return grpc.Errorf(codes.Unauthenticated, "Missing context metadata")
}
