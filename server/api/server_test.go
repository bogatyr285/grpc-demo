package srv_test

import (
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
	"testing"

	Client "bitbucket.org/bogatyr285/grpc_demo/client/api"
	pb "bitbucket.org/bogatyr285/grpc_demo/proto"
	ServerFactory "bitbucket.org/bogatyr285/grpc_demo/server/api"
)

func init() {
	//go to root directory. get rid of err about relative paths to certs
	_, filename, _, _ := runtime.Caller(0)
	dir := path.Join(path.Dir(filename), "../..")
	err := os.Chdir(dir)
	if err != nil {
		panic(err)
	}

	srvAddr := "localhost"
	srvPort := "10001"
	server, err := ServerFactory.NewServer(srvAddr, srvPort)
	if err != nil {
		log.Fatalf("Failed to inialize gRPC server: %v", err)
	}
	go func() {
		server.Start()
	}()
}

func TestClient(t *testing.T) {
	srvAddr := "localhost:10001"
	client, err := Client.NewClient(srvAddr)
	if err != nil {
		t.Fatalf("Failed to inialize gRPC client: %v", err)
	}
	client.Start()

	t.Run("Unary request", func(t *testing.T) {
		reply, err := client.SendMsg("Tester", "Testing msg")
		if err != nil {
			t.Fatalf("Failed to send message: %v", err)
		}
		t.Logf("Got answer from the server: %+v", reply)
	})
	t.Run("Server streaming", func(t *testing.T) {
		//init server streaming
		streamResChan, errChan := client.GetStreamRes("TEST req to stream")
		if err != nil {
			t.Fatalf("Failed init server streaming %v", err)
		}

	getStreamLoop:
		for {
			select {
			case msg := <-streamResChan:
				t.Logf("GetStreamRes received message:%+v\n", msg)
			case err := <-errChan:
				if err != nil {
					t.Fatalf("GetStreamRes error:%+v\n", err)
				} else {
					t.Logf("End of stream\n")
				}
				break getStreamLoop
			}
		}
	})

	t.Run("Client streaming", func(t *testing.T) {
		//prepare client streaming
		clntReqs := []*pb.StreamMsgRequest{}
		for i := 0; i < 3; i++ {
			clntReqs = append(clntReqs, &pb.StreamMsgRequest{Body: fmt.Sprintf("TEST Client req is: %d", i)})
		}
		//client streaming
		replyToStream := client.SendStreamReq(clntReqs)
		if err != nil {
			t.Fatalf("Failed client side streaming: %v", err)
		}
		t.Logf("Server's answer to client stream: %+v", replyToStream)
	})
}
